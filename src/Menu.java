import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;


public class Menu extends Scene
{	
	private boolean isPressed = false;
	
	private String[] menu;
	private int selected;
	private animation currentAnimation;
	private int animationProgress; // over 100
	private nextMenu direction; // up
	private String[] backMenu = null;
	private String[] info = { "", "", "" };
	
	private String[] mainMenu = { "New Game", "Load Game", "Options", "Exit"};
	private String[] mainMenuR = { "Resume Game", "New Game", "Load Game", "Options", "Exit"};
	private String[] options = { "Graphics"};
	private String[] graphics = { "Resolution", "Full Screen", "Text Quality", "Vsync", "Color Quality"};
	private String[] resolutions = {};
	private String[] fullScreenMenu = { "On", "Off" };
	private String[] textQuality = { "Low", "High" };
	private String[] vsync = { "Off", "On (30 fps)", "On (50 fps)", "On (60 fps)" };
	private String[] colorQuality = {};
	private String[] loadGameDialog = { };
	
	private enum animation { FADEOUT, FADEIN, TODOWN, TOUP, ZIP, UNZIP, FREEZE, TORIGHT, TOLEFT, CREATEPROFILE}
	private enum nextMenu { UP, DOWN, ANOTHER, BACK, NEW }
	
	private Sprite menuGrad;
	private String profileName = "";
	private boolean shiftPressed = false;
	
	public Menu()
	{
		super();
		
		currentAnimation = animation.FREEZE;
		animationProgress = 0;
		menu = mainMenu;
		
		loadProfile();
		
		menuGrad = new Sprite("menu","png");

		
		gameLoop();
	}
	
	
	
	@Override
	public void draw()
	{
		switch(currentAnimation)
		{
		case FREEZE:
			Text.draw(700, 80, getOption(selected-3));
			Text.draw(700, 160, getOption(selected-2));
			Text.draw(700, 240, getOption(selected-1));
			Text.draw(700, 320, getOption(selected), new Color(0f,1f,0f));
			Text.draw(700, 400, getOption(selected+1));
			Text.draw(700, 480, getOption(selected+2));
			Text.draw(700, 560, getOption(selected+3));
			
			Text.draw2(100, 350, info[0]);
			Text.draw2(100, 380, info[1]);
			Text.draw2(100, 410, info[2]);
			break;
		case ZIP:
			Text.draw(700, 320-(100-animationProgress)*2.4f, getOption(selected-3));
			Text.draw(700, 320-(100-animationProgress)*1.6f, getOption(selected-2));
			Text.draw(700, 320-(100-animationProgress)*0.8f, getOption(selected-1));
			Text.draw(700, 320, getOption(selected));
			Text.draw(700, 320+(100-animationProgress)*0.8f, getOption(selected+1));
			Text.draw(700, 320+(100-animationProgress)*1.6f, getOption(selected+2));
			Text.draw(700, 320+(100-animationProgress)*2.4f, getOption(selected+3));
			break;
		case UNZIP:
			Text.draw(700, 320-animationProgress*2.4f, getOption(selected-3));
			Text.draw(700, 320-animationProgress*1.6f, getOption(selected-2));
			Text.draw(700, 320-animationProgress*0.8f, getOption(selected-1));
			Text.draw(700, 320, getOption(selected));
			Text.draw(700, 320+animationProgress*0.8f, getOption(selected+1));
			Text.draw(700, 320+animationProgress*1.6f, getOption(selected+2));
			Text.draw(700, 320+animationProgress*2.4f, getOption(selected+3));
			break;
		case FADEOUT:
			Text.draw(700, 80, getOption(selected-3));
			Text.draw(700, 160, getOption(selected-2));
			Text.draw(700, 240, getOption(selected-1));
			Text.draw(700, 320, getOption(selected), new Color(0.01f*animationProgress,1f,0.01f*animationProgress));
			Text.draw(700, 400, getOption(selected+1));
			Text.draw(700, 480, getOption(selected+2));
			Text.draw(700, 560, getOption(selected+3));
			
			Text.draw2(100 -5*animationProgress, 350, info[0]);
			Text.draw2(100 -5*animationProgress, 380, info[1]);
			Text.draw2(100 -5*animationProgress, 410, info[2]);
			break;
		case TODOWN:
			Text.draw(700, 80-0.8f*animationProgress, getOption(selected-3));
			Text.draw(700, 160-0.8f*animationProgress, getOption(selected-2));
			Text.draw(700, 240-0.8f*animationProgress, getOption(selected-1));
			Text.draw(700, 320-0.8f*animationProgress, getOption(selected));
			Text.draw(700, 400-0.8f*animationProgress, getOption(selected+1));
			Text.draw(700, 480-0.8f*animationProgress, getOption(selected+2));
			Text.draw(700, 560-0.8f*animationProgress, getOption(selected+3));
			Text.draw(700, 640-0.8f*animationProgress, getOption(selected+4));
			break;
		case FADEIN:
			if(animationProgress == 0)
				setInfo();
			
			Text.draw(700, 80, getOption(selected-3));
			Text.draw(700, 160, getOption(selected-2));
			Text.draw(700, 240, getOption(selected-1));
			Text.draw(700, 320, getOption(selected), new Color(0.01f*(100-animationProgress),1f,0.01f*(100-animationProgress)));
			Text.draw(700, 400, getOption(selected+1));
			Text.draw(700, 480, getOption(selected+2));
			Text.draw(700, 560, getOption(selected+3));
			
			Text.draw2(-400 +5*animationProgress, 350, info[0]);
			Text.draw2(-400 +5*animationProgress, 380, info[1]);
			Text.draw2(-400 +5*animationProgress, 410, info[2]);
			break;
		case TOUP:
			Text.draw(700, 0.8f*animationProgress, getOption(selected-4));
			Text.draw(700, 80+0.8f*animationProgress, getOption(selected-3));
			Text.draw(700, 160+0.8f*animationProgress, getOption(selected-2));
			Text.draw(700, 240+0.8f*animationProgress, getOption(selected-1));
			Text.draw(700, 320+0.8f*animationProgress, getOption(selected));
			Text.draw(700, 400+0.8f*animationProgress, getOption(selected+1));
			Text.draw(700, 480+0.8f*animationProgress, getOption(selected+2));
			Text.draw(700, 560+0.8f*animationProgress, getOption(selected+3));
			break;
		case TORIGHT:
			Text.draw(700+5*animationProgress, 320, getOption(selected-3), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			Text.draw(700+5*animationProgress, 320, getOption(selected-2), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			Text.draw(700+5*animationProgress, 320, getOption(selected-1), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			Text.draw(700+5*animationProgress, 320, getOption(selected), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			Text.draw(700+5*animationProgress, 320, getOption(selected+1), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			Text.draw(700+5*animationProgress, 320, getOption(selected+2), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			Text.draw(700+5*animationProgress, 320, getOption(selected+3), new Color(1-0.01f*animationProgress,1-0.01f*animationProgress,1-0.01f*animationProgress));
			break;
		case TOLEFT:
			if(animationProgress<5 && menu == mainMenu && game!=null)
			{
				menu = mainMenuR;
			}
				
			Text.draw(1200-5*animationProgress, 320, getOption(selected-3), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			Text.draw(1200-5*animationProgress, 320, getOption(selected-2), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			Text.draw(1200-5*animationProgress, 320, getOption(selected-1), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			Text.draw(1200-5*animationProgress, 320, getOption(selected), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			Text.draw(1200-5*animationProgress, 320, getOption(selected+1), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			Text.draw(1200-5*animationProgress, 320, getOption(selected+2), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			Text.draw(1200-5*animationProgress, 320, getOption(selected+3), new Color(0.01f*animationProgress,0.01f*animationProgress,0.01f*animationProgress));
			break;
		case CREATEPROFILE:
			Text.draw(450, 130, "Profile name:");
			
			Shape.setColor(1f, 1f, 1f);
			Shape.drawRectangle(440, 200, 400, 40);
			Shape.setColor(0f, 0f, 0f);
			Shape.drawRectangle(445, 205, 390, 30);
			
			Text.draw2(450,206, profileName);
			
			Text.draw(300,500, info[0], Color.red);
			Text.draw(340,580, info[1], Color.red);	
			break;
		default:
			break;
		}
		
		
		if(currentAnimation != animation.CREATEPROFILE)
		{
			menuGrad.draw(650, 0);	// menu gradient
		}


	}
	
	@Override
	public void move()
	{
		switch(currentAnimation)
		{
		case ZIP:
			if((animationProgress+=9) > 100)
			{
				currentAnimation = animation.TORIGHT;
				animationProgress = 0;
			}
			break;
		case UNZIP:
			if((animationProgress+=7) > 100)
			{
				currentAnimation = animation.FADEIN;
				selected = 0;
				animationProgress = 0;
			}
			break;
		case FADEOUT:
			if((animationProgress+=26) > 100)
			{
				animationProgress = 0;
				switch(direction)
				{
				case UP:
					currentAnimation = animation.TOUP;
					break;
				case DOWN:
					currentAnimation = animation.TODOWN;
					break;
				case ANOTHER:
					currentAnimation = animation.ZIP;
					break;
				case BACK:
					currentAnimation = animation.ZIP;
					break;
				case NEW:
					currentAnimation = animation.CREATEPROFILE;
					break;
				}
			}
			break;
		case TODOWN:
			if((animationProgress+=17) > 100)
			{
				currentAnimation = animation.FADEIN;
				selected++;
				animationProgress = 0;
			}
			break;
		case FADEIN:
			if((animationProgress+=3) > 100)
			{
				currentAnimation = animation.FREEZE;
				animationProgress = 0;
			}
			break;
		case TOUP:
			if((animationProgress+=17) > 100)
			{
				currentAnimation = animation.FADEIN;
				selected--;
				animationProgress = 0;
			}
			break;
		case TORIGHT:
			if((animationProgress+=4) > 100)
			{
				animationProgress = 0;
				currentAnimation = animation.TOLEFT;
				
				if(direction == nextMenu.ANOTHER)
					select();
				else if(backMenu != null)
				{
					menu = backMenu;
					backMenu = mainMenu;
				}
			}			
			break;
		case TOLEFT:
			if(animationProgress <5)
				selected = 0;
			if((animationProgress+=4) > 100)
			{
				animationProgress = 0;
				currentAnimation = animation.UNZIP;
			}			
			break;
		default:
			break;
		}
	}
	
	@Override
	public void input()
	{
		if(Keyboard.next())
		{
			if(isPressed)
			{
				isPressed = false;
			}
			else if((currentAnimation==animation.FREEZE || currentAnimation==animation.FADEIN))
			{
				isPressed = true;
				
				if(Keyboard.getEventKey() == Keyboard.KEY_S || Keyboard.getEventKey() == Keyboard.KEY_DOWN)
				{
					direction = nextMenu.DOWN;
					currentAnimation = animation.FADEOUT;
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_W || Keyboard.getEventKey() == Keyboard.KEY_UP)
				{
					direction = nextMenu.UP;
					currentAnimation = animation.FADEOUT;
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_RETURN)
				{
					if(getOption(selected).equals("Create New Profile") || getOption(selected).equals("New Game"))
						direction = nextMenu.NEW;
					else
						direction = nextMenu.ANOTHER;
					
					currentAnimation = animation.FADEOUT;				
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
				{
					direction = nextMenu.BACK;
					currentAnimation = animation.FADEOUT;				
				}
			}
			else if(currentAnimation == animation.CREATEPROFILE)
			{
				info[0] = "";
				info[1] = "";
				
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
				{
					isPressed = true;
					
					direction = nextMenu.BACK;
					currentAnimation = animation.TOLEFT;				
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_RETURN)
				{
					isPressed = true;
					
					for(String str:loadGameDialog)
					{
						if(str.equals(">> "+profileName))
						{
							info[0] = "This profile name is already taken.";
							info[1] = "Try to write different name.";							
							return ;
						}
					}
					
					Profile.player = new Profile(profileName, true);
					game = new Game();
					inGame = true;
					currentAnimation = animation.FREEZE;
					menu = mainMenuR;
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_BACK)
				{
					isPressed = true;
					
					if(profileName.length()>0)
						profileName = profileName.substring(0, profileName.length()-1);
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_LSHIFT || Keyboard.getEventKey() == Keyboard.KEY_RSHIFT)
				{
					if(Keyboard.getEventKeyState())
						shiftPressed = true;
					else
						shiftPressed = false;
				}
				else
				{
					isPressed = true;
					
					if(profileName.length() < 20)
					{
						if(Keyboard.getEventCharacter() < 'a' || Keyboard.getEventCharacter() > 'z')
							if(Keyboard.getEventCharacter() < 'A' || Keyboard.getEventCharacter() > 'Z')
								if(Keyboard.getEventCharacter() < '0' || Keyboard.getEventCharacter() > '9')
									if(Keyboard.getEventCharacter() != '-' && Keyboard.getEventCharacter() != '_' && Keyboard.getEventCharacter() != ' ')
										return ;
							
						if(shiftPressed)
						{
							profileName += Character.toUpperCase(Keyboard.getEventCharacter());
						}
						else
						{
							profileName += Keyboard.getEventCharacter();
						}
					}
				}
			}
		}
	}
	
	private void select()
	{
		while(selected < 0)
			selected += menu.length;
		
		while(selected >= menu.length)
			selected -= menu.length;
		
		
		if(menu == mainMenu)
		{
			switch(selected)
			{
			case 0:
				backMenu = mainMenu;
				break;				
			case 1:
				loadProfile();
				menu = loadGameDialog;
				backMenu = mainMenu;
				break;
			case 2:
				menu = options;
				backMenu = mainMenu;
				selected =  0;
				break;
			case 3:
				exitFromGame = true;
				break;
			}
		}
		else if(menu == mainMenuR)
		{
			switch(selected)
			{
			case 0:
				backMenu = mainMenu;
				inGame = true;
				break;
			case 1:
				backMenu = mainMenu;
				break;				
			case 2:
				loadProfile();
				menu = loadGameDialog;
				backMenu = mainMenu;
				break;
			case 3:
				menu = options;
				backMenu = mainMenu;
				selected =  0;
				break;
			case 4:
				exitFromGame = true;
				break;
			}
		}
		else if(menu == options)
		{
			switch(selected)
			{
			case 0:
				if(resolutions.length <2)
					getResolutions();
				backMenu = options;
				menu = graphics;
			}
		}
		else if(menu == graphics)
		{
			switch(selected)
			{
			case 0:
				backMenu = graphics;
				menu = resolutions;
				selected =  0;
				break;
			case 1:
				backMenu = graphics;
				menu = fullScreenMenu;
				selected =  0;
				break;
			case 2:
				backMenu = graphics;
				menu = textQuality;
				selected =  0;
				break;
			case 3:
				backMenu = graphics;
				menu = vsync;
				selected =  0;
				break;				
			case 4:
				backMenu = graphics;
				menu = colorQuality;
				selected = 0;
				break;
			}
		}
		else if(menu == resolutions)
		{
			String[] res = resolutions[selected].split("x");
			
			Settings.width = Integer.parseInt(res[0]);
			Settings.height = Integer.parseInt(res[1]);
			Settings.saveFile();
			
			backMenu = options;
			menu = graphics;
			selected =  0;
		}
		else if(menu == fullScreenMenu)
		{
			if(selected == 1)
				Settings.isFullScreen = false;
			else
				Settings.isFullScreen = true;
			
			Settings.saveFile();
			backMenu = options;
			menu = graphics;
			selected =  0;
		}
		else if(menu == vsync)
		{
			switch(selected)
			{
				case 0:
					Settings.vsync = 1000;
					break;
				case 1:
					Settings.vsync = 30;
					break;
				case 2:
					Settings.vsync = 50;
					break;
				case 3:
					Settings.vsync = 60;
					break;
			}
			
			Settings.saveFile();
			backMenu = options;
			menu = graphics;
			selected =  0;
		}
		else if(menu == colorQuality)
		{
			if(menu[selected].equals("Very Low"))
				Settings.colorDepth = 8;
			else if(menu[selected].equals("Low"))
				Settings.colorDepth = 16;
			else if(menu[selected].equals("Medium"))
				Settings.colorDepth = 24;
			else if(menu[selected].equals("High"))
				Settings.colorDepth = 32;
			
			Settings.saveFile();
			backMenu = options;
			menu = graphics;
			selected =  0;
		}
		else if(menu == loadGameDialog)
		{
			if(selected == 0)
			{
				backMenu = mainMenu;
			}
			else
			{
				String str = menu[selected].substring(3);
				Profile.player = new Profile(str, false);
				game = new Game();
				inGame = true;
				menu = mainMenuR;
			}
		}
		
	}
	
	private String getOption(int x)
	{
		while(x < 0)
			x += menu.length;
		
		while(x >= menu.length)
			x -= menu.length;
		
		return menu[x];
	}
	
	private void getResolutions()
	{
		org.lwjgl.opengl.DisplayMode[] modes = null;
		ArrayList<String> resolutionList = new ArrayList<String>();
		ArrayList<String> colors = new ArrayList<String>();
		
		try
		{
			modes = Display.getAvailableDisplayModes();
		}
		catch (LWJGLException e)
		{
			Error.code(2);
		}
		
		org.lwjgl.opengl.DisplayMode nativeMode = new org.lwjgl.opengl.DisplayMode((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth(), (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight());
		
		if(nativeMode.isFullscreenCapable())
			resolutionList.add(nativeMode.getWidth()+"x"+nativeMode.getHeight());
		
		for(org.lwjgl.opengl.DisplayMode mode:modes)
		{
			switch(mode.getBitsPerPixel())
			{
			case 8:
				if(!colors.contains("Very Low"))
					colors.add("Very Low");
				break;
			case 16:
				if(!colors.contains("Low"))
					colors.add("Low");
				break;
			case 24:
				if(!colors.contains("Medium"))
					colors.add("Medium");
				break;
			case 32:
				if(!colors.contains("High"))
					colors.add("High");
				break;
			}
			
			if(mode.isFullscreenCapable() && mode.getWidth()/(float)mode.getHeight()>1.4 && !resolutionList.contains(mode.getWidth()+"x"+mode.getHeight()))
				resolutionList.add(mode.getWidth()+"x"+mode.getHeight());
		}
			
				
		resolutions = resolutionList.toArray(resolutions);
		Arrays.sort(resolutions);
		
		colorQuality = colors.toArray(colorQuality);

	}
	
	public void setInfo()
	{
		info[0] = "";
		info[1] = "";
		info[2] = "";
				
		while(selected <0)
			selected += menu.length;
		
		if(menu == graphics)
		{
			switch(selected%menu.length)
			{
			case 0:
				info[1] = "Recommanded: "+ (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth() +"x"+ (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight(); 
				break;
			case 2:
				info[0] = "Font smoothing";
				info[1] = "Recommanded: High";
				break;
			case 3:
				info[0] = "To prevent from screen tearing";
				info[1] = "Recommanded: On (50 FPS)";
				break;
			case 4:
				info[0] = "Color depth";
				info[1] = "Strictly Recommanded : "+colorQuality[colorQuality.length-1];
				break;
			}
		}
		else if(menu == options)
		{
			info[0] = "You may need to restart game";
			info[1] =  "to apply changes.";
		}
	}
	
	private void loadProfile()
	{
		ArrayList<String> profileNames = new ArrayList<String>();
		profileNames.add("Create New Profile");
		
		File file = new File("cfg");	
		String[] savedList = file.list(Profile.fileFilter());
	
		for(String str:savedList)
		{
			str = ">> "+str.substring(2, str.length()-4);
			profileNames.add(str);
		}
		
		loadGameDialog = profileNames.toArray(loadGameDialog);
	}
	
}
