
public class GameLoop extends Thread
{
	private Scene game;
	
	public GameLoop(Scene game)
	{
		this.game = game;
	}
	
	public void run()
	{
		long time = System.currentTimeMillis();
		long difference;
			
		
		while(true)
		{
			difference = System.currentTimeMillis() - time;
			
			if(difference<20)
				try
				{
					sleep(20-difference);
				}
				catch (InterruptedException e)
				{ }
			
			time = System.currentTimeMillis();
			
			if(Scene.inGame)
				game.game.move();
			else
				game.move();
		}
	}
}
