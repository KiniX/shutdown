import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Start
{
	public static Scene scene;

	public static void main(String[] args)
	{
		startProgram();
	}
	
	private static void startProgram()
	{
		int width = 800;
		int height = 450;
				
		try
		{
			BufferedReader stream = new BufferedReader(new FileReader("cfg/settings.knx"));
			width = stream.read()*128;
			width += stream.read()%128;
			height = stream.read()*128;
			height += stream.read()%128;
			
			int data = stream.read();
			// data: fs-aa-vs-vs-
			
			if(data%2 == 1)
				Settings.setDisplay(width, height, true);
			else
				Settings.setDisplay(width, height, false);
			
			
			if((data/2)%2 == 1)
				Settings.antiAliasing = true;
			
			switch((data/4)%4)
			{
			case 0:
				Settings.vsync = 1000;	// off
				break;
			case 1:
				Settings.vsync = 60;
				break;
			case 2:
				Settings.vsync = 50;
				break;
			case 3:
				Settings.vsync = 30;
				break;
			}
			
			data = stream.read();
			
			switch(data%4)
			{
			case 0:
				Settings.colorDepth = 8;
				break;
			case 1:
				Settings.colorDepth = 16;
				break;
			case 2:
				Settings.colorDepth = 24;
				break;
			case 3:
				Settings.colorDepth = 32;
				break;
			}
			
			stream.close();

		}
		catch (IOException e)
		{
			width = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
			height = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
			
			Settings.setDisplay(width, height, true);
			Settings.antiAliasing = true;
			Settings.vsync = 50;
			Settings.colorDepth = 32;
		}
		
		
		scene = new Menu();	
	}
	

}
