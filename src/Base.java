
public class Base
{
	public int x;
	public int y;
	public int health;
	
	public Base(int x, int y)
	{
		this.x = x;
		this.y = y;
		health = 10;
	}
	
	public void draw()
	{
		if(health == 10)
			Global.baseB.draw(x, y);
		else
			Global.baseR.draw(x, y);
	}
}
