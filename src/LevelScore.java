
public class LevelScore
{
	public int score;
	public boolean money;
	public boolean perfect;
	
	public LevelScore()
	{
		score = 0;
		money = false;
		perfect = false;
	}

	public void replay(int point, boolean madalM, boolean madalB)
	{
		
		if(this.score < point)
		{
			switch(Profile.player.chosenTowerforLevel)
			{
			case 1:
				while(100 < (point-score)/(Profile.player.turretLevel+1) + Profile.player.turretProgress)
				{
					if(Profile.player.turretLevel < 3)
					{
						Profile.player.turretLevel++;
						
						if(Profile.player.turretLevel == 3)
							Profile.player.turretProgress = 100;
						else
							Profile.player.turretProgress = (point-score)/(Profile.player.turretLevel+1) - (100-Profile.player.turretProgress);
					}
				}				
				Profile.player.turretProgress += (point-score)/(Profile.player.turretLevel+1);

				break;
			case 2:
				while(100 < (point-score)/(Profile.player.gunLevel+1) + Profile.player.gunProgress)
				{
					if(Profile.player.gunLevel < 3)
					{
						Profile.player.gunLevel++;
						
						if(Profile.player.gunLevel == 3)
							Profile.player.gunProgress = 100;
						else
							Profile.player.gunProgress = (point-score)/(Profile.player.gunLevel+1) - (100-Profile.player.gunProgress);
					}
				}				
				Profile.player.gunProgress += (point-score)/(Profile.player.gunLevel+1);

				break;
			case 3:
				while(100 < (point-score)/(Profile.player.teslaLevel+1) + Profile.player.laserProgress)
				{
					if(Profile.player.teslaLevel < 3)
					{
						Profile.player.teslaLevel++;
						
						if(Profile.player.teslaLevel == 3)
							Profile.player.laserProgress = 100;
						else
							Profile.player.laserProgress = (point-score)/(Profile.player.teslaLevel+1) - (100-Profile.player.laserProgress);
					}
				}				
				Profile.player.laserProgress += (point-score)/(Profile.player.teslaLevel+1);

				break;
			case 4:
				while(100 < (point-score)/(Profile.player.moneyLevel+1) + Profile.player.moneyProgress)
				{
					if(Profile.player.moneyLevel < 3)
					{
						Profile.player.moneyLevel++;
						
						if(Profile.player.moneyLevel == 3)
							Profile.player.moneyProgress = 100;
						else
							Profile.player.moneyProgress = (point-score)/(Profile.player.moneyLevel+1) - (100-Profile.player.moneyProgress);
					}
				}				
				Profile.player.moneyProgress += (point-score)/(Profile.player.moneyLevel+1);

				break;
			}

			
			score = point;
		}
		
		if(!money)
			money = madalM;
		
		if(!perfect)
			perfect = madalB;
	}
}
