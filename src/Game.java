import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;


public class Game
{
	public static boolean inGame;
	private int chosenLevel;
	public static Map map;
	private boolean isPressed;
	

	private int hoverButton = 0; // 1-4:upgrade; 5:play; 6-7:levelChange;
	
	public Game()
	{
		isPressed = false;
		inGame = false;
		chosenLevel = Profile.player.maxLevel;
		
		Global.scale(false);
	}
	
	public void draw()
	{
		if(inGame)
		{
			map.draw();
		}
		else
		{
			// left main
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(60, 40, 700, 540);
			Shape.setColor(0.1f, 0.5f, 0f);
			Shape.drawRectangle(65, 45, 690, 530);
			
			// right main
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(770, 40, 440, 540);
			Shape.setColor(0.1f, 0.5f, 0f);
			Shape.drawRectangle(775, 45, 430, 530);
			
			// left map button
			if(chosenLevel>1)
			{
				Shape.setColor(0.3f, 0.7f, 0.1f);
				Shape.drawRectangle(790, 60, 50, 50);
				if(hoverButton == 6)
					Shape.setColor(0.1f, 0.3f, 0.1f);
				else
					Shape.setColor(0f, 0.2f, 0f);
				Shape.drawRectangle(795, 65, 40, 40);
				Text.draw(800, 55, "<");
			}
			
			// right map button
			if(chosenLevel<Profile.player.maxLevel)
			{
				Shape.setColor(0.3f, 0.7f, 0.1f);
				Shape.drawRectangle(1140, 60, 50, 50);
				if(hoverButton == 7)
					Shape.setColor(0.1f, 0.3f, 0.1f);
				else
					Shape.setColor(0f, 0.2f, 0f);
				Shape.drawRectangle(1145, 65, 40, 40);
				Text.draw(1150, 55, ">");
			}
			
			// level name
			Text.draw2(950,70,"Level "+chosenLevel);
			Text.draw2(900, 200, "Score : "+ Profile.player.scores.get(chosenLevel-1).score);
			Text.draw2(900, 400, "Madals :");
			if(Profile.player.scores.get(chosenLevel-1).money)
				Global.money.draw(1000, 380);
			if(Profile.player.scores.get(chosenLevel-1).perfect)
				Global.baseB.draw(1080, 340);
			
			
			// left menu
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(100, 170, 75, 75);
			if(hoverButton == 1)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(105, 175, 65, 65);
			Global.turret.draw(105, 175);
			Text.draw2(200, 175, "Max.Level: "+Profile.player.turretLevel);
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(200, 210, 510, 35);
			if(hoverButton == 1)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(205, 215, 500, 25);
			Shape.setColor(0.2f, 0.8f, 0f);
			Shape.drawRectangle(205, 215, (int)(5*Profile.player.turretProgress), 25);
						
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(100, 270, 75, 75);
			if(hoverButton == 2)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(105, 275, 65, 65);
			Global.gunTower.draw(105, 275);
			Text.draw2(200, 275, "Max.Level: "+Profile.player.gunLevel);
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(200, 310, 510, 35);
			if(hoverButton == 2)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(205, 315, 500, 25);
			Shape.setColor(0.2f, 0.8f, 0f);
			Shape.drawRectangle(205, 315, (int)(5*Profile.player.gunProgress), 25);
			
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(100, 370, 75, 75);
			if(hoverButton == 3)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(105, 375, 65, 65);
			Global.tesla.draw(105, 375);
			Text.draw2(200, 375, "Max.Level: "+Profile.player.teslaLevel);
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(200, 410, 510, 35);
			if(hoverButton == 3)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(205, 415, 500, 25);
			Shape.setColor(0.2f, 0.8f, 0f);
			Shape.drawRectangle(205, 415, (int)(5*Profile.player.laserProgress), 25);
			
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(100, 470, 75, 75);
			if(hoverButton == 4)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(105, 475, 65, 65);
			Global.money.draw(105, 475);
			Text.draw2(200, 475, "Money Level: "+Profile.player.moneyLevel);
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(200, 510, 510, 35);
			if(hoverButton == 4)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(205, 515, 500, 25);
			Shape.setColor(0.2f, 0.8f, 0f);
			Shape.drawRectangle(205, 515, (int)(5*Profile.player.moneyProgress), 25);
			
			// chosen tower
			if(Profile.player.chosenTowerforLevel != 0)
				Text.draw(80, 60+100*Profile.player.chosenTowerforLevel, ".");
			
			// bottom button
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(860, 590, 350, 85);
			if(hoverButton == 5)
				Shape.setColor(0.1f, 0.3f, 0.1f);
			else
				Shape.setColor(0f, 0.2f, 0f);
			Shape.drawRectangle(865, 595, 340, 75);
			Text.draw(930, 600, "PLAY >>");
			
			// bottom info background
			Shape.setColor(0.3f, 0.7f, 0.1f);
			Shape.drawRectangle(60, 590, 790, 85);
			Shape.setColor(0.1f, 0.5f, 0f);
			Shape.drawRectangle(65, 595, 780, 75);
			
			// bottom info text
			if(hoverButton>0 && hoverButton<5)
			{
				Text.draw2(80, 600, "The points you gain will affect the levelling of the tower of your choice.");
				if(hoverButton==4)
					Text.draw2(80, 630, "Higher money level means earning more money for each kill.");
				else
					Text.draw2(80, 630, "You should choose a tower or the money.");
					
			}
		}
		
	}
	
	
	public void move()
	{
		if(inGame)
		{
			map.move();
		}
	}
	
	
	public void input()
	{
		int mousex = (int)(Mouse.getX()*Scene.xRatio);
		int mousey = 720-(int)(Mouse.getY()*Scene.yRatio);
		
		hoverButton = 0;
		
		Global.keyNext = false;
		if(Keyboard.next())
		{
			if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
			{
				Scene.inGame = false;
			}
			Global.keyNext = true;
		}
		
		if(inGame)
		{
			map.input();
		}
		else
		{
			if(mousex > 100 && mousex < 710)
			{
				if(mousey > 170 && mousey < 245)
					hoverButton = 1;
				else if(mousey > 270 && mousey < 345)
					hoverButton = 2;
				else if(mousey > 370 && mousey < 445)
					hoverButton = 3;
				else if(mousey > 470 && mousey < 545)
					hoverButton = 4;
			}
			else if(mousex > 860 && mousex < 1210 && mousey > 590 && mousey < 675)
				hoverButton = 5;
			else if(mousex > 790 && mousex < 840 && mousey > 60 && mousey < 110)
				hoverButton = 6;
			else if(mousex > 1140 && mousex < 1190 && mousey > 60 && mousey < 110)
				hoverButton = 7;
		}
		
		
		if(Mouse.isButtonDown(0) && !isPressed)
		{
			isPressed = true;
			
			switch(hoverButton)
			{
			case 1:
				Profile.player.chosenTowerforLevel = 1;
				break;
			case 2:
				Profile.player.chosenTowerforLevel = 2;
				break;
			case 3:
				Profile.player.chosenTowerforLevel = 3;
				break;
			case 4:
				Profile.player.chosenTowerforLevel = 4;
				break;
			case 5:
				map = new Map(chosenLevel);
				inGame = true;
				break;
			case 6:
				if(chosenLevel>1)
					chosenLevel--;
				break;
			case 7:
				if(chosenLevel<8)
					chosenLevel++;
				break;
			}
		}
		else if(!Mouse.isButtonDown(0))
		{
			isPressed = false;
		}
		
	}
}
