
public class Wave
{
	public int spider;
	public int tank;
	
	public Wave()
	{
		spider = 0;
		tank = 0;
	}
	
	public void addSpider(int x)
	{
		spider += x;
	}
	
	public void addTank(int x)
	{
		tank += x;
	}
}
