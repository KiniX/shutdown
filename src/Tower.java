
public class Tower
{
	public int x, y;
	public int direction; // 0:top
	public float angle;
	public int coolTime;
	public Monster target;
	
	public int damage;
	public float distLimit;
	public int coolLimit;
	
	public int level;
	public int cost;
	
	public Tower(int turretX, int turretY)
	{
		level = 0;
		Game.map.grid[turretX][turretY] = true;
		x = turretX*64;
		y = (turretY+2)*64;
	}
	
	
	public void draw()
	{
		
	}
	
	public void move()
	{
		float edge1, edge2, distance;
		
		if(target == null)
		{
			for(Monster monster:Game.map.tanks)
			{
				edge1 = monster.x - x;
				edge2 = monster.y - y;
				distance = (float)Math.sqrt(edge1*edge1 + edge2*edge2);
				
				if(distance < distLimit)
				{
					target = monster;
					
					break;
				}
			}
			
			
			if(target == null)
				for(Monster monster:Game.map.spiders)
				{
					edge1 = monster.x - x;
					edge2 = monster.y - y;
					distance = (float)Math.sqrt(edge1*edge1 + edge2*edge2);
					
					if(distance < distLimit)
					{
						target = monster;
						
						break;
					}
				}
			
			
		}
		
		if(target != null)
		{
			edge1 = target.x - x;
			edge2 = target.y - y;
			
			distance = (float)Math.sqrt(edge1*edge1 + edge2*edge2);
			
			if(distance > distLimit)
			{
				target = null;
				return ;
			}

			angle = (float)(Math.toDegrees(Math.atan(edge2/edge1)));
			

			if(edge1>0)
			{
				if(angle<0)
					angle = 180+angle;
				else
					angle = angle-180;
			}
				
			
			if(angle < -157.5)
				direction = 2;
			else if(angle < -112.5)
				direction = 3;
			else if(angle < -67.5)
				direction = 4;
			else if(angle < -22.5)
				direction = 5;
			else if(angle < 22.5)
				direction = 6;
			else if(angle < 67.5)
				direction = 7;
			else if(angle < 112.5)
				direction = 0;
			else if(angle < 157.5)
				direction = 1;
			else
				direction = 2;
			
		}
		

	}


	public void upgrade()
	{
		if(level>2)
		{
			Game.map.info1 = "This tower already reached max level.";
			Game.map.info2 = "";
			Game.map.infoTime = 150;
			return ;
		}
		
		if((this instanceof Turret && Profile.player.turretLevel>level)
				||
			(this instanceof GunTower && Profile.player.gunLevel>level)
				||
			(this instanceof LaserTower && Profile.player.teslaLevel>level))
		{	
			
			if(Game.map.money < cost)
			{
				Game.map.info1 = "You do not have enough money.";
				Game.map.info2 = "You need $"+cost;
				Game.map.infoTime = 120;
			}
			else
			{
				Game.map.money -= cost;
				
				level++;
				
				damage += damage/(level*2);
				distLimit += distLimit/(level*2);
			}
		
		}
		else
		{
			Game.map.info1 = "You cannot upgrade to this level.";
			Game.map.info2 = "";
			Game.map.infoTime = 150;
		}
	}
	
	public static boolean check(int tX, int tY)
	{
		int x = 0;
		int y = 128;
		
		if(tX > 0 && tY > 0 && tY < 9)
			Game.map.grid[tX][tY] = true;
		
		int grid[][] = new int[21][8];
		
		for(int i=0; i<21; i++)
			for(int j=0; j<8; j++)
				grid[i][j] = 100;
				
		grid[(int)(x/64)][(int)(y/64)-2] = 0;
				
		boolean flag = false;
		boolean found = false;
				
		while(!flag && !found)
		{
			flag = true;
			
			for(int i=0; i<21; i++)
				for(int j=0; j<8; j++)
					if(grid[i][j]<100)
					{	
						if(found)
							break;
						
						for(Base base:Game.map.bases)
						{

							if((base.x/64 == i && base.y/64 == j+1) || (base.x/64 == i && base.y/64 == j+2))
							{

								int index;
								
								while(!found)
								{										
									index = 4;
									
									if(Game.map.type == 2)
										if(j == 4)
											if(i == 5)
												i = 15;
											else if(i == 15)
												i = 5;
									
									if(j>0)
										if(grid[i][j-1] == grid[i][j]-1)
										{
											index = 0;
										}
									
									if(i<20)
										if(grid[i+1][j] == grid[i][j]-1)
										{
											index = 1;
										}	
									
									if(j<7)
										if(grid[i][j+1] == grid[i][j]-1)
										{
											index = 2;
										}
									
									if(i>0)
										if(grid[i-1][j] == grid[i][j]-1)
										{
											index = 3;
										}

									
									switch(index)
									{
									case 0:
										j--;
										break;
									case 1:
										i++;
										break;
									case 2:
										j++;
										break;
									case 3:
										i--;
										break;
									}
									
									if(grid[i][j] == 0)
									{
										
										found = true;
									}
									
								}								

							}
						}
						
						if(i>0)
							if(grid[i-1][j] > grid[i][j]+1  && !Game.map.grid[i-1][j])
							{
								grid[i-1][j] = grid[i][j]+1;
								flag = false;
							}
						
						if(i<20)
							if(grid[i+1][j] > grid[i][j]+1 && !Game.map.grid[i+1][j])
							{
								grid[i+1][j] = grid[i][j]+1;
								flag = false;
							}
						
						if(j>0)
							if(grid[i][j-1] > grid[i][j]+1 && !Game.map.grid[i][j-1])
							{
								grid[i][j-1] = grid[i][j]+1;
								flag = false;
							}
						
						if(j<7)
							if(grid[i][j+1] > grid[i][j]+1 && !Game.map.grid[i][j+1])
							{
								grid[i][j+1] = grid[i][j]+1;
								flag = false;
							}
											
					}
		}
		
		if(!found)
		{
			if(tX > 0 && tY > 0 && tY < 9)
				Game.map.grid[tX][tY] = false;
			return false;
		}
		
		x = 1216;
		y = 128;
		
		grid = new int[21][8];
		
		for(int i=0; i<21; i++)
			for(int j=0; j<8; j++)
				grid[i][j] = 100;
				
		grid[(int)(x/64)][(int)(y/64)-2] = 0;
				
		flag = false;
		found = false;
				
		while(!flag && !found)
		{
			flag = true;
			
			for(int i=0; i<21; i++)
				for(int j=0; j<8; j++)
					if(grid[i][j]<100)
					{	
						if(found)
							break;
						
						for(Base base:Game.map.bases)
						{

							if((base.x/64 == i && base.y/64 == j+1) || (base.x/64 == i && base.y/64 == j+2))
							{

								int index;
								
								while(!found)
								{										
									index = 4;
									
									if(Game.map.type == 2)
										if(j == 4)
											if(i == 5)
												i = 15;
											else if(i == 15)
												i = 5;
									
									if(j>0)
										if(grid[i][j-1] == grid[i][j]-1)
										{
											index = 0;
										}
									
									if(i<20)
										if(grid[i+1][j] == grid[i][j]-1)
										{
											index = 1;
										}	
									
									if(j<7)
										if(grid[i][j+1] == grid[i][j]-1)
										{
											index = 2;
										}
									
									if(i>0)
										if(grid[i-1][j] == grid[i][j]-1)
										{
											index = 3;
										}

									
									switch(index)
									{
									case 0:
										j--;
										break;
									case 1:
										i++;
										break;
									case 2:
										j++;
										break;
									case 3:
										i--;
										break;
									}
									
									if(grid[i][j] == 0)
									{
										
										found = true;
									}
									
								}								

							}
						}
						
						if(i>0)
							if(grid[i-1][j] > grid[i][j]+1  && !Game.map.grid[i-1][j])
							{
								grid[i-1][j] = grid[i][j]+1;
								flag = false;
							}
						
						if(i<20)
							if(grid[i+1][j] > grid[i][j]+1 && !Game.map.grid[i+1][j])
							{
								grid[i+1][j] = grid[i][j]+1;
								flag = false;
							}
						
						if(j>0)
							if(grid[i][j-1] > grid[i][j]+1 && !Game.map.grid[i][j-1])
							{
								grid[i][j-1] = grid[i][j]+1;
								flag = false;
							}
						
						if(j<7)
							if(grid[i][j+1] > grid[i][j]+1 && !Game.map.grid[i][j+1])
							{
								grid[i][j+1] = grid[i][j]+1;
								flag = false;
							}
											
					}
		}
		
		if(tX > 0 && tY > 0 && tY < 9)
			Game.map.grid[tX][tY] = false;
		return found;
	}
	
}
