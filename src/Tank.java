
public class Tank extends Monster
{
	
	public Tank()
	{
		super();
		
		health = 250;
	}
	
	
	public void draw()
	{
		Shape.setColor(1f, 0f, 0f);
		Shape.drawRectangle((int)x+2, (int)y, 60, 2);
		Shape.setColor(0f, 1f, 0f);
		Shape.drawRectangle((int)x+2, (int)y, (int)(60*health/(250*Global.healthMulti)), 2);
		
		
		switch(direction)
		{
		case 0:
			Global.tank.draw(x, y, 2+animation/120, 0);
			break;
		case 1:
			Global.tank.draw(x, y, 2+animation/120, 1);
			break;
		case 2:
			Global.tank.draw(x, y, animation/120, 0);
			break;
		case 3:
			Global.tank.draw(x, y, animation/120, 1);
			break;
		}
		
	}
}
