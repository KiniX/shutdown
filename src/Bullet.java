
public class Bullet
{
	public float x, y, tX, tY;
	public float speedX, speedY;
	public int damage;
	boolean active;
	public Monster target;
	public int animation;
	
	public Bullet(int x, int y, Monster target, int damage)
	{
		this.x = x+32;
		this.y = y+32;
		this.tX = target.x+32;
		this.tY = target.y+32;
		this.target = target;
		this.damage = damage;
		
		active = true;
	}
	
	
	public void draw()
	{
		
	}
	
	public void move()
	{

	}
}
