import javax.swing.JOptionPane;


public class Error
{
	public static void code(int x)
	{
		code(x, "");
	}
	
	public static void code(int x, String str)
	{
		String error = "";
		boolean exit = true;
		
		switch(x)
		{
		case 0:
			error = "The settings file is not valid. Please restart game for default settings.";
			break;
		case 1:
			error = str+" is not found!";
			break;
		case 2:
			error = "There is no display modes for your adapter.";
			break;
		case 3:
			exit = false;
			error = "Permission denied while saving settings.";
			break;
		case 4:
			error = "Files of level "+str+" cannot be read.";
			break;
		case 5:
			error = "Profile files are corrupt.";
			break;
		case 6:
			error = "Permission denied while saving profile.";
		}
		
		JOptionPane.showMessageDialog(null, error, "ERROR:"+x, JOptionPane.ERROR_MESSAGE);
		
		if(exit)
			System.exit(0);
	}
}
