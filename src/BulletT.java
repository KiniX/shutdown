
public class BulletT extends Bullet
{
	
	

	public BulletT(int x, int y, Monster target, int damage)
	{
		super(x, y, target, damage);
		
		speedX = (tX-x)/16;
		speedY = (tY-y)/16;
		
		animation = 0;
	}
	

	public void move()
	{
		super.move();
		
		
		animation += Global.speed;
		
		x += speedX*Global.speed;
		y += speedY*Global.speed;
		
		if(animation>15)
		{
			target.health = target.health-damage;
			active = false;
		}
	}
	
	public void draw()
	{
		Shape.setColor(1f, 1f, 0.5f);
		Shape.drawRectangle((int)x, (int)y, 3, 3);
		Shape.setColor(0.5f, 0.2f, 0f);
		Shape.drawRectangle((int)x+1, (int)y+1, 1, 1);
	}
}
