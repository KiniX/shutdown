import java.io.IOException;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sprite
{
	private Texture texture;
	private int width;
	private int height;
	private float scale;
	private float imgx;
	private float imgy;
	
	public Sprite(String name, String type)
	{
		try
		{
			texture = TextureLoader.getTexture(type, ResourceLoader.getResourceAsStream("img/"+name+"."+type));
		}
		catch (IOException e)
		{
			Error.code(0,name+"."+type);
		}
		
		width = texture.getTextureWidth();
		height = texture.getTextureHeight();
		imgx = 1;
		imgy = 1;
		scale = 1;
	}
	
	public void finalize()
	{
		texture.release();
	}
	
	public void setResolution(int width,int height)
	{
		imgx = (float)width / this.width;
		imgy = (float)height / this.height;
		this.width = width;
		this.height = height;
	}
	
	public void scale(int width)
	{
		this.scale = (float)width/this.width;
	}

	public void draw(float x, float y)
	{
		draw(x,y,0,0);
	}
	
	public void draw(float x, float y, int ox, int oy)
	{
		texture.bind();

		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(imgx*ox,imgy*oy);
		GL11.glVertex2f(x,y);
		GL11.glTexCoord2f(imgx*(ox+1),imgy*oy);
		GL11.glVertex2f(x+width*scale,y);
		GL11.glTexCoord2f(imgx*(ox+1),imgy*(oy+1));
		GL11.glVertex2f(x+width*scale,y+height*scale);
		GL11.glTexCoord2f(imgx*ox,imgy*(oy+1));
		GL11.glVertex2f(x,y+height*scale);
		GL11.glEnd();		
	}
	
	
}
