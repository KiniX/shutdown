
public class Global
{
	public static int speed = 1;
	public static float healthMulti;
	
	public static Sprite gunTower = new Sprite("gun", "png");
	public static Sprite turret = new Sprite("turret", "png");
	public static Sprite tesla = new Sprite("tesla", "png");
	public static Sprite baseR = new Sprite("baser","png");
	public static Sprite baseB = new Sprite("baseb","png");
	public static Sprite wall = new Sprite("wall","png");
	public static Sprite floor = new Sprite("floor","gif");
	public static Sprite spider = new Sprite("spider","png");
	public static Sprite tank = new Sprite("tank","png");
	public static Sprite money = new Sprite("money", "png");
	public static Sprite explosion = new Sprite("explosion", "png");
	public static Sprite portal = new Sprite("portal","png");
	public static boolean keyNext;
	
	
	public static void initSprite()
	{
		gunTower.setResolution(118, 118);
		turret.setResolution(118, 118);
		tesla.setResolution(118, 118);
		spider.setResolution(118, 118);
		tank.setResolution(118, 118);
		explosion.setResolution(118, 118);
		portal.setResolution(125, 126);
	}
	
	public static void scale(boolean small)
	{
		if(small)
		{
			gunTower.scale(32);
			turret.scale(32);
			tesla.scale(32);
			spider.scale(32);
			tank.scale(32);
			explosion.scale(32);
			portal.scale(32);
		}
		else
		{
			gunTower.scale(64);
			turret.scale(64);
			tesla.scale(64);
			spider.scale(64);
			tank.scale(64);
			explosion.scale(64);
			portal.scale(64);
		}
	}
}
