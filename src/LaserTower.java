
public class LaserTower extends Tower
{

	public LaserTower(int turretX, int turretY)
	{
		super(turretX, turretY);
		
		distLimit = 120;
		direction = 0;
		
		coolLimit = 60;
		coolTime = 1;
		damage = 50;

		cost = 325;
	}
	
	
	public void move()
	{
		super.move();
		
		if((coolTime += Global.speed) > coolLimit)
		{
			coolTime = 0;
			
			if(target != null)
			{
				Game.map.bullets.add(new BulletL(x,y, target, damage));
				
				if(target.health<0)
					target = null;
			}
		}
	}
	
	
	public void draw()
	{
		switch(direction)
		{
		case 0:
			Global.tesla.draw(x, y, 0, 1);
			break;
		case 1:
			Global.tesla.draw(x, y, 1, 1);
			break;
		case 2:
			Global.tesla.draw(x, y, 2, 1);
			break;
		case 3:
			Global.tesla.draw(x, y, 3, 1);
			break;
		case 4:
			Global.tesla.draw(x, y, 0, 0);
			break;
		case 5:
			Global.tesla.draw(x, y, 1, 0);
			break;
		case 6:
			Global.tesla.draw(x, y, 2, 0);
			break;
		case 7:
			Global.tesla.draw(x, y, 3, 0);
			break;
		}
	}
}
