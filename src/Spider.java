
public class Spider extends Monster
{

	public Spider()
	{
		super();
		
		health = (int) (100*Global.healthMulti);
	}
	

	
	public void draw()
	{
		Shape.setColor(1f, 0f, 0f);
		Shape.drawRectangle((int)x+2, (int)y, 60, 2);
		Shape.setColor(0f, 1f, 0f);
		Shape.drawRectangle((int)x+2, (int)y, (int)(60*health/(100*Global.healthMulti)), 2);
		switch(direction)
		{
		case 0:
			Global.spider.draw(x, y, animation/80, 0);
			break;
		case 1:
			Global.spider.draw(x, y, animation/80, 1);
			break;
		case 2:
			Global.spider.draw(x, y, animation/80, 3);
			break;
		case 3:
			Global.spider.draw(x, y, animation/80, 2);
			break;
		}
		
	}
}
