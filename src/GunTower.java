
public class GunTower extends Tower
{

	public GunTower(int turretX, int turretY)
	{
		super(turretX, turretY);
		
		distLimit = 200;
		direction = 0;
		
		coolLimit = 250;
		coolTime = 1;
		damage = 100;

		cost = 250;
	}
	
	public void move()
	{
		super.move();
		
		if((coolTime += Global.speed) > coolLimit)
		{
			coolTime = 0;
			
			if(target != null)
			{
				Game.map.bullets.add(new BulletG(x,y, target, damage));
				
				if(target.health<0)
					target = null;
			}
		}
	}
	
	
	public void draw()
	{
		switch(direction)
		{
		case 0:
			Global.gunTower.draw(x, y, 0, 1);
			break;
		case 1:
			Global.gunTower.draw(x, y, 1, 1);
			break;
		case 2:
			Global.gunTower.draw(x, y, 2, 1);
			break;
		case 3:
			Global.gunTower.draw(x, y, 3, 1);
			break;
		case 4:
			Global.gunTower.draw(x, y, 0, 0);
			break;
		case 5:
			Global.gunTower.draw(x, y, 1, 0);
			break;
		case 6:
			Global.turret.draw(x, y, 2, 0);
			break;
		case 7:
			Global.gunTower.draw(x, y, 3, 0);
			break;
		}
	}
}
