import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;


public class Profile
{
	public static Profile player;
	
	public String name;
	public int maxLevel;
	public int gunLevel;
	public int turretLevel;
	public int teslaLevel;
	public int moneyLevel;
	
	public float turretProgress;
	public float gunProgress;
	public float laserProgress;
	public float moneyProgress;	
	
	public int chosenTowerforLevel;
	
	public ArrayList<LevelScore> scores;
	
	public Profile(String name, boolean isNew)
	{
		this.name = name;
		
		scores = new ArrayList<LevelScore>();
		
		if(isNew)
		{
			maxLevel = 1;
			gunLevel = 0;
			turretLevel = 0;
			teslaLevel = 0;
			moneyLevel = 0;
			chosenTowerforLevel = 1;
			
			turretProgress = 0;
			gunProgress = 0;
			laserProgress = 0;
			moneyProgress = 0;
			
			scores.add(new LevelScore());
			
			saveFile();
		}
		else
		{
			try
			{
				LevelScore temp;
				int data;
				
				BufferedReader stream = new BufferedReader(new FileReader("cfg/p_"+name+".knx"));
				maxLevel = stream.read();
				gunLevel = stream.read();
				turretLevel = stream.read();
				teslaLevel = stream.read();
				moneyLevel = stream.read();
				chosenTowerforLevel = stream.read();
				
				turretProgress = stream.read();
				turretProgress += stream.read()/127.0;
				gunProgress = stream.read();
				gunProgress += stream.read()/127.0;
				laserProgress = stream.read();
				laserProgress += stream.read()/127.0;
				moneyProgress = stream.read();
				moneyProgress += stream.read()/127.0;
				
				for(int i=0; i<maxLevel; i++)
				{
					temp = new LevelScore();
					temp.score = stream.read();
					temp.score += stream.read()*128;
					
					data = stream.read();
					if(data%2 == 1)
						temp.perfect = true;
					else 
						temp.perfect = false;
					
					if((data/2)%2 == 1)
						temp.money = true;
					else 
						temp.money = false;
					
					scores.add(temp);
				}
				
				stream.close();
			}
			catch (IOException e)
			{
				Error.code(5);
			}
		}
	}
	
	public void saveFile()
	{
		FileWriter file;
		
		try
		{
			file = new FileWriter("cfg/p_"+name+".knx");
			file.write(maxLevel);
			file.write(gunLevel);
			file.write(turretLevel);
			file.write(teslaLevel);
			file.write(moneyLevel);
			file.write(chosenTowerforLevel);
			
			file.write((int)turretProgress);
			file.write((int)(turretProgress*127)%127);
			file.write((int)gunProgress);
			file.write((int)(gunProgress*127)%127);
			file.write((int)laserProgress);
			file.write((int)(laserProgress*127)%127);
			file.write((int)moneyProgress);
			file.write((int)(moneyProgress*127)%127);
			
			int data;
			
			for(int i=0; i<maxLevel; i++)
			{
				file.write(scores.get(i).score%127);
				file.write(scores.get(i).score/127);
				
				data = 0;
				if(scores.get(i).perfect)
					data+=1; 
				
				if(scores.get(i).money)
					data+=3; 
				
				file.write(data);
			}
			
			
			file.close();
		}
		catch (IOException e)
		{
			Error.code(6);
		}
	}
	
	public static FilenameFilter fileFilter()
	{
		return new FilenameFilter()
		{

			@Override
			public boolean accept(File dir, String name)
			{
				if(name.endsWith(".knx") && name.startsWith("p_"))
					return true;
				return false;
			}
		};
	}
	
}
