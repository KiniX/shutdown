import org.lwjgl.opengl.GL11;


public class Shape
{
	private static float r = 1;
	private static float g = 1;
	private static float b = 1;
	
	public static void setColor(float _r, float _g, float _b)
	{
		r = _r;
		g = _g;
		b = _b;
	}
	
	public static void drawRectangle(int x, int y, int w, int h)
	{
		GL11.glColor3f(r, g, b);
		GL11.glDisable(GL11.GL_TEXTURE_2D);  
		
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2f(x,y);
		GL11.glVertex2f(x+w,y);
		GL11.glVertex2f(x+w,y+h);
		GL11.glVertex2f(x,y+h);
		GL11.glEnd();	
		
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);  
		GL11.glColor3f(1f,1f,1f);
	}
}
