import java.io.File;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;


public class Scene
{
	public static boolean inGame;
	public Game game;
	public static float xRatio;
	public static float yRatio;
	public GameLoop moveLoop;
	public static boolean exitFromGame = false;
	
	public Scene()
	{
		try
		{
			
			DisplayMode displayMode = null;
			DisplayMode[] modes = Display.getAvailableDisplayModes();
				        
			for (int i = 0; i < modes.length; i++)
			{				
				if (modes[i].getWidth() == Settings.width && modes[i].getHeight() == Settings.height && modes[i].isFullscreenCapable())
				{
					displayMode = modes[i];
					
					if(modes[i].getBitsPerPixel() == Settings.colorDepth)
						break;
				}
			}
	     			   
			
	        Display.setDisplayMode(displayMode);
			Display.setFullscreen(Settings.isFullScreen);
			
			Display.create();
			
			GL11.glEnable(GL11.GL_TEXTURE_2D);               
			GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
			
        	GL11.glEnable(GL11.GL_BLEND);
        	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

			GL11.glLoadIdentity();
			GL11.glOrtho(0, 1280, 720, 0, 1, -1);
			
			Global.initSprite();
			
			xRatio = 1280f/Settings.width;
			yRatio = 720f/Settings.height;
		}
		catch (LWJGLException e)
		{
			new File("cfg/settings.knx").delete();
			Error.code(0);
		}

		inGame = false;
	}
	
	@SuppressWarnings("deprecation")
	public void gameLoop()
	{
		moveLoop = new GameLoop(this);
		moveLoop.start();
		
		long time = System.currentTimeMillis();
		long difference;
		int limitTime = 1000/Settings.vsync;
		
		if(Settings.vsync == 60)
			limitTime++;
		
		while(!Display.isCloseRequested() && !exitFromGame)
		{
			difference = System.currentTimeMillis() - time;
			
			if(difference<limitTime)
				try
				{
					Thread.sleep(limitTime-difference);
				}
				catch (InterruptedException e)
				{ }
			
			time = System.currentTimeMillis();

			if(inGame)
			{
				game.input();
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
				game.draw();
			}
			else
			{
				input();
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
				draw();
			}
				
			Display.update();
		}
		
		Display.destroy();
		moveLoop.interrupt();
		moveLoop.stop();
		
	}
	
	public void input() {}
	public void move() {}
	public void draw() {}
}
