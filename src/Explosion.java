
public class Explosion
{
	public int animation;
	private int x, y;
	
	public Explosion(int x, int y)
	{
		animation = 0;
		this.x = x;
		this.y = y;
	}
	
	public void move()
	{
		animation += Global.speed;
	}
	
	public void draw()
	{
		Global.explosion.draw(x,y, animation/5 , 0);
	}
}
