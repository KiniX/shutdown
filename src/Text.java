import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;




public class Text
{
	private static Font awtFont = new Font("Times New Roman", Font.BOLD, 54);
	private static Font awtFont2 = new Font("Times New Roman", Font.BOLD, 24);
	private static TrueTypeFont font = new TrueTypeFont(awtFont, Settings.antiAliasing);
	private static TrueTypeFont font2 = new TrueTypeFont(awtFont2, Settings.antiAliasing);
	private static int defaultSize = 54;
	
	public static void setSize(int x)
	{
		if(defaultSize == x)
			return ;
		
		awtFont = new Font("Times New Roman", Font.BOLD, x);
		defaultSize = x;
		font = new TrueTypeFont(awtFont, Settings.antiAliasing);		
	}
	
	public static void draw(float x, float y, String str, Color color)
	{	
		
		font.drawString(x,y, str, color);
	}
	
	public static void draw(float x, float y, String str)
	{
		draw(x,y,str,Color.white);
	}
	
	public static void draw2(float x, float y, String str, Color color)
	{
		font2.drawString(x,y, str, color);
	}
	
	public static void draw2(float x, float y, String str)
	{
		draw2(x,y,str,Color.white);
	}

}
