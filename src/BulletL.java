import org.lwjgl.opengl.GL11;


public class BulletL  extends Bullet
{

	
	public BulletL(int x, int y, Monster target, int damage)
	{
		super(x, y, target, damage);
		
		animation = 0;
	}
	
	public void move()
	{
		animation += Global.speed;
		
		if(animation > 10)
		{
			target.health = target.health-damage;
			active = false;
		}
			
		
	}
	
	public void draw()
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);  
		
		GL11.glLineWidth(5);
		GL11.glColor3f(1f, 1f, 0f);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2f(x, y);
		GL11.glVertex2f(tX, tY);
		GL11.glEnd();
		
		GL11.glLineWidth(3);
		GL11.glColor3f(1f, 0.2f, 0f);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2f(x, y);
		GL11.glVertex2f(tX, tY);
		GL11.glEnd();
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);  
		GL11.glColor3f(1f,1f,1f);
		
	}
	

}
