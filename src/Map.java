import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;


public class Map
{
	private boolean isPressed;
	private boolean isClicked;
	
	public boolean[][] grid;
	public int type; // 0:normal 1:transport 2:portal
	
	public int point;
	private int moneyMadalLimit;
	public int money;
	
	private int currentWave;
	private int createdSpider;
	private int createdTank;
	
	public ArrayList<Wave> waves = new ArrayList<Wave>();
	public ArrayList<Base> bases = new ArrayList<Base>();
	public ArrayList<Spider> spiders = new ArrayList<Spider>();
	public ArrayList<Tank> tanks = new ArrayList<Tank>();
	public ArrayList<Tower> towers = new ArrayList<Tower>();
	public ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	
	public ArrayList<Explosion> explosions = new ArrayList<Explosion>();
	
	public int counter;
	public int counterLimit;
	
	private int chosenButton = 0; // 4:upgrade 5:sell
	private int chosenTurret = 0; // 1:turret 2:guntower 3:laser
	private Tower infoTower;
	
	private int turretX = 0;
	private int turretY = 0;
	
	private int level;
	public String info1;
	public String info2;
	public int infoTime;
	
	public Map(int level)
	{
		this.level = level;
		isPressed = false;
		isClicked = false;
		infoTime = 0;
		
		point = 0;
		
		grid = new boolean[21][8];
		int temp1, temp2, temp3;
		
		currentWave = 0;
		counter = 0;
		
		for(int i=0; i<21; i++)
			for(int j=0; j<8; j++)
				grid[i][j] = false;
		
		try
		{
			BufferedReader stream = new BufferedReader(new FileReader("lvl/"+level+".lvl"));
			
			temp1 = stream.read();
		
			for(int i=0; i<temp1; i++)
			{
				temp2 = stream.read();
				temp3 = stream.read();
				
				bases.add(new Base(temp2*64,(temp3+2)*64));
			}
			
			type = stream.read();
			
			temp1 = stream.read();
			
			for(int i=0; i<temp1; i++)
			{
				waves.add(new Wave());
				waves.get(i).addSpider(stream.read());
				waves.get(i).addTank(stream.read());
			}
			
			counterLimit = stream.read();
			
			Global.healthMulti = stream.read()/10.0f;
			money = stream.read()*100;
			
			moneyMadalLimit = stream.read()*100;
			
			stream.close();			
		}
		catch (IOException e)
		{
			Error.code(4,""+level);
		}
		
	}
	
	public void draw()
	{
		drawHUD();
		
		for(int i=0; i<1280; i+=32)
		{
			Global.wall.draw(i, 96);
			
			for(int j=128; j<640; j+=32)
				Global.floor.draw(i, j);
			
			Global.wall.draw(i, 640);
		}
		
		// portal
		if(type == 2)
		{

			Global.portal.draw(320,384,0,0);
			Global.portal.draw(960,384,0,1);

		}
		
		if(chosenTurret != 0)
		{
			boolean canTowerable = true;
			
			if(type==2)
				if((turretX == 5 || turretX == 15) && turretY == 4)
					canTowerable = false;
			
			for(Base base:bases)
				if(base.x/64 == turretX && (base.y/64 == turretY +2 || base.y/64 == turretY +1))
					canTowerable = false;
			
			if(turretX != -1)
				if(grid[turretX][turretY])
					canTowerable = false;
				
			if(!canTowerable)
				Shape.setColor(1f, 0.5f, 0.5f);
			else
				Shape.setColor(0.5f, 1f, 0.5f);
			
			Shape.drawRectangle(turretX*64, (turretY+2)*64, 64, 64);
		}
		
		for(Base base:bases)
		{
			base.draw();
		}
		
		
		for(Spider spider:spiders)
		{
			spider.draw();
		}
		
		for(Tank tank:tanks)
		{
			tank.draw();
		}
		
		for(Tower tower:towers)
		{
			tower.draw();
		}
		
		for(Bullet bullet:bullets)
		{
			bullet.draw();
		}
		
		for(Explosion exp:explosions)
		{
			exp.draw();
		}
		
		if(infoTower != null)
		{
			Shape.setColor(1f, 1f, 1f);
			Shape.drawRectangle(infoTower.x, infoTower.y, 64, 64);
			if(chosenButton == 4)
				Shape.setColor(0.7f, 0.7f, 0.7f);
			else
				Shape.setColor(0.4f, 0.4f, 0.4f);
			Shape.drawRectangle(infoTower.x+2, infoTower.y+2, 60, 28);
			if(chosenButton == 5)
				Shape.setColor(0.7f, 0.7f, 0.7f);
			else
				Shape.setColor(0.4f, 0.4f, 0.4f);
			Shape.drawRectangle(infoTower.x+2, infoTower.y+34, 60, 28);
			
			if(infoTower.level == 0 || infoTower.level == 2)
				Text.draw2(infoTower.x+22, infoTower.y+2, "*");
			if(infoTower.level == 1 || infoTower.level == 2)
				Text.draw2(infoTower.x+12, infoTower.y+2, "* *");		
			if(infoTower.level == 3)
				Text.draw2(infoTower.x+12, infoTower.y+2, "MAX");
			Text.draw2(infoTower.x+23, infoTower.y+33, "$");
		}
		
		// tower cost
		Shape.setColor(0f, 0f, 0f);
		if(chosenButton == 1)
		{
			Shape.drawRectangle(20, 90, 60, 30);
			Text.draw2(20,90, "$100");
		}
		else if(chosenButton == 2)
		{
			Shape.drawRectangle(100, 90, 60, 30);
			Text.draw2(100,90, "$250");
		}
		else if(chosenButton == 3)
		{
			Shape.drawRectangle(180, 90, 60, 30);
			Text.draw2(180,90, "$325");
		}
		
	}
	
	
	public void move()
	{
		if(infoTime>0)
			infoTime--;
		
		if(spiders.size() == 0 && tanks.size() == 0 && createdSpider==waves.get(currentWave).spider && createdTank==waves.get(currentWave).tank)
		{
			currentWave++;
			createdSpider = 0;
			createdTank = 0;
			
			if(currentWave == waves.size())
			{
				for(Tower tower:towers)
					money += (tower.level+1)*tower.cost;
				
				boolean madalM = false;
				if(money>moneyMadalLimit)
					madalM = true;
				
				boolean madalB = true;
				for(Base base:bases)
					if(base.health<100)
						madalB = false;
				
				point += money;
				
				for(Base base:bases)
				{
					point += base.health;
				}
				
				Profile.player.scores.get(level-1).replay(point , madalM, madalB);
				
				if(Profile.player.maxLevel == level && level != 8)
				{
					Profile.player.maxLevel++;
					Profile.player.scores.add(new LevelScore());
				}
				Profile.player.saveFile();
				
				Game.inGame = false;
			}
			
		}
		
		if((counter+=Global.speed) > counterLimit)
		{
			counter = 0;
			
			if(createdSpider < waves.get(currentWave).spider)
			{
				spiders.add(new Spider());
				createdSpider++;
			}
				
			
			if(createdTank < waves.get(currentWave).tank)
			{
				tanks.add(new Tank());
				createdTank++;
			}
		}
		
		try
		{
			for(Spider spider:spiders)
			{
				if(spider.active)
				{
					spider.move();
					for(Base base:bases)
					{
						if(base.x - spider.x > -64 && base.x - spider.x < 64)
							if(base.y - spider.y > -128 && base.y - spider.y < 64)
							{
								if(base.health-- < 1)
								{
									bases.remove(base);
								}
								explosions.add(new Explosion((int)spider.x, (int)spider.y));
								spiders.remove(spider);
								break;
							}
					}
				}
				else
				{
					money += 25*(1+Profile.player.moneyLevel/5.0);
					point += 2;
					spiders.remove(spider);
				}
			}
		}
		catch(ConcurrentModificationException e)
		{
			
		}
		
		try
		{
			for(Tank tank:tanks)
			{
				if(tank.active)
				{
					tank.move();
					for(Base base:bases)
					{
						if(base.x - tank.x > -64 && base.x - tank.x < 64)
							if(base.y - tank.y > -128 && base.y - tank.y < 64)
							{
								if(base.health-- < 1)
								{
									bases.remove(base);
								}
								explosions.add(new Explosion((int)tank.x, (int)tank.y));
								tanks.remove(tank);
								break;
							}
					}
				}
				else
				{
					money += 42*(1+Profile.player.moneyLevel/5.0);
					point += 3;
					tanks.remove(tank);
				}
			}
		}
		catch(ConcurrentModificationException e)
		{
			
		}
		
		try
		{
			for(Tower tower:towers)
			{
				tower.move();
			}
		}
		catch(ConcurrentModificationException e)
		{
			
		}
		
		try
		{
			for(Bullet bullet:bullets)
			{
				if(bullet.active)
					bullet.move();
				else
					bullets.remove(bullet);
			}
		}
		catch(ConcurrentModificationException e)
		{

		}
		
		try
		{
			for(Explosion exp:explosions)
			{
				exp.move();
				
				if(exp.animation > 24)
					explosions.remove(exp);
			}
		}
		catch(ConcurrentModificationException e)
		{

		}
	}
	
	private void drawHUD()
	{
		float progress = 100;
		if(waves.get(currentWave).spider!=0 && waves.get(currentWave).tank!=0)
			progress -= (waves.get(currentWave).spider-createdSpider)/(float)waves.get(currentWave).spider + (waves.get(currentWave).tank-createdTank)/(float)waves.get(currentWave).tank*100;
		else if(waves.get(currentWave).spider!=0)	
			progress -= (waves.get(currentWave).spider-createdSpider)/(float)waves.get(currentWave).spider*100;
		else
			progress -= (waves.get(currentWave).tank-createdTank)/(float)waves.get(currentWave).tank*100;
		
		if(progress<0)
			progress = 0;
		
		// wave
		Text.draw2(300, 680, "Wave "+(currentWave+1)+"/"+waves.size());
		Shape.setColor(0.4f, 0.4f, 0.4f);
		Shape.drawRectangle(440, 680, 604, 24);
		Shape.setColor(0f, 0f, 0f);
		Shape.drawRectangle(442, 682, 600, 20);
		Shape.setColor(0f, 0.6f, 0f);
		Shape.drawRectangle(442, 682, (int)(6*progress), 20);
		
		// play/pause/ffw
		GL11.glDisable(GL11.GL_TEXTURE_2D);  
		
		switch(Global.speed)
		{
		case 0:
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(250,680);
			GL11.glVertex2f(260,680);
			GL11.glVertex2f(260,704);
			GL11.glVertex2f(250,704);
			GL11.glEnd();	
			
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(270,680);
			GL11.glVertex2f(280,680);
			GL11.glVertex2f(280,704);
			GL11.glVertex2f(270,704);
			GL11.glEnd();
			break;
		case 1:
			GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
			GL11.glVertex2f(250,680);
			GL11.glVertex2f(280,692);
			GL11.glVertex2f(250,704);
			GL11.glEnd();
			break;
		case 4:
			GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
			GL11.glVertex2f(250,680);
			GL11.glVertex2f(270,692);
			GL11.glVertex2f(250,704);
			GL11.glEnd();
			GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
			GL11.glVertex2f(260,680);
			GL11.glVertex2f(280,692);
			GL11.glVertex2f(260,704);
			GL11.glEnd();
			break;
		}

		
		GL11.glEnable(GL11.GL_TEXTURE_2D);  
		
		// top tower menu
		if(chosenTurret == 1)
			Shape.setColor(0.5f, 0.3f, 0f);
		else
			Shape.setColor(0.5f, 0.2f, 0f);
		Shape.drawRectangle(10, 10, 76, 76);
		if(chosenButton == 1)
			Shape.setColor(0.2f, 0.1f, 0f);
		else
			Shape.setColor(0f, 0f, 0f);
		Shape.drawRectangle(15, 15, 66, 66);
		Global.turret.draw(16, 16);
		
		if(chosenTurret == 2)
			Shape.setColor(0.5f, 0.3f, 0f);
		else
			Shape.setColor(0.5f, 0.2f, 0f);
		Shape.drawRectangle(90, 10, 76, 76);
		if(chosenButton == 2)
			Shape.setColor(0.2f, 0.1f, 0f);
		else
			Shape.setColor(0f, 0f, 0f);
		Shape.drawRectangle(95, 15, 66, 66);
		Global.gunTower.draw(96, 16);			
		
		if(chosenTurret == 3)
			Shape.setColor(0.5f, 0.3f, 0f);
		else
			Shape.setColor(0.5f, 0.2f, 0f);
		Shape.drawRectangle(170, 10, 76, 76);
		if(chosenButton == 3)
			Shape.setColor(0.2f, 0.1f, 0f);
		else
			Shape.setColor(0f, 0f, 0f);
		Shape.drawRectangle(175, 15, 66, 66);
		Global.tesla.draw(176, 16);		
		
		// info
		if(infoTime > 44)
		{
			Shape.setColor(0.5f, 0.2f, 0f);
			Shape.drawRectangle(258, 0, 788, 87);
			Shape.setColor(0.2f, 0.1f, 0f);
			Shape.drawRectangle(262, 0, 780, 83);
			Text.draw2(270, 10, info1);
			Text.draw2(270, 44, info2);
		}
		else if(infoTime>0)
		{
			Shape.setColor(0.5f, 0.2f, 0f);
			Shape.drawRectangle(258, (infoTime-44)*2, 788, 87);
			Shape.setColor(0.2f, 0.1f, 0f);
			Shape.drawRectangle(262, (infoTime-44)*2, 780, 83);
			Text.draw2(270, (infoTime-39)*2, info1);
			Text.draw2(270, (infoTime-22)*2, info2);
		}
		
		// point
		Text.draw2(1100, 8, "Point: "+point);
		Text.draw2(1100, 32, "Money: $"+money);
		int health = 0;
		for(Base base:bases)
			health += base.health;
		Text.draw2(1100, 56, "Health: "+health);
		
		if(health < 1)
			Game.inGame = false;
	}
	
	
	public void input()
	{
		int mousex = (int)(Mouse.getX()*Scene.xRatio);
		int mousey = 720-(int)(Mouse.getY()*Scene.yRatio);
		
		chosenButton = 0;
		turretX = -1;
		turretY = -1;
		
		
		
		if(Global.keyNext)
		{
			if(isPressed)
			{
				isPressed = false;
			}
			else
			{
				isPressed = true;
				if(Keyboard.getEventKey() == Keyboard.KEY_SPACE)
					if(Global.speed == 0)
						Global.speed = 1;
					else
						Global.speed = 0;
				else if(Keyboard.getEventKey() == Keyboard.KEY_LSHIFT || Keyboard.getEventKey() == Keyboard.KEY_RSHIFT)
					if(Global.speed == 4)
						Global.speed = 1;
					else
						Global.speed = 4;
			}
		}
		

		
		if(mousey>10 && mousey<86)
		{
			if(mousex>10 && mousex<86)
				chosenButton = 1;
			else if(mousex>90 && mousex<166)
				chosenButton = 2;
			else if(mousex>170 && mousex<246)
				chosenButton = 3;
		}
		else if(mousey>128 && mousey<640)
		{
			turretX = mousex/64;
			turretY = mousey/64 - 2;
			
			if(infoTower != null)
			{
				if(mousex - infoTower.x < 64 && mousex - infoTower.x >0)
				{
					if(mousey - infoTower.y>0 && mousey - infoTower.y<32)
						chosenButton = 4;
					else if(mousey - infoTower.y>32 && mousey - infoTower.y<64)
						chosenButton = 5;
				}
			}
		}
		
		
		if(Mouse.isButtonDown(0) && !isClicked)
		{		
			isClicked = true;
			
			boolean canTowerable = true;
			infoTower = null;
			if(chosenTurret>0 && chosenTurret<4)
			{
				for(Base base:bases)
					if(base.x/64 == turretX && (base.y/64 == turretY +2 || base.y/64 == turretY +1))
						canTowerable = false;
				
				if(type==2)
					if((turretX == 5 || turretX == 15) && turretY == 4)
						canTowerable = false;
								
				if(turretX != -1)
				{
					if(grid[turretX][turretY])
						canTowerable = false;
				}
				else
					canTowerable = false;
			}
			else
			{
				for(Tower tower:towers)
				{
					if(turretX*64 == tower.x && (turretY+2)*64 == tower.y)
					{
						infoTower = tower;
						break;
					}
				}
			}
			
			if(infoTower != null)
			{
				if(chosenButton == 4)
				{
					infoTower.upgrade();
					infoTower = null;
				}
				else if(chosenButton == 5)
				{
					money += (infoTower.level+1)*infoTower.cost;
					grid[infoTower.x/64][(infoTower.y-128)/64] = false;
					towers.remove(infoTower);
					infoTower = null;
				}
				
			}
			
			
			if(canTowerable && Tower.check(turretX, turretY))
				switch(chosenTurret)
				{
				case 1:
					if(money>=100)
					{
						towers.add(new Turret(turretX,turretY));
						money -= 100;
					}
					else
					{
						info1 = "You do not have enough money.";
						info2 = "You need $100";
						infoTime = 120;
					}
					break;
				case 2:
					if(money>=250)
					{
						towers.add(new GunTower(turretX, turretY));
						money -= 250;
					}
					else
					{
						info1 = "You do not have enough money.";
						info2 = "You need $250";
						infoTime = 120;
					}
					break;
				case 3:
					if(money>=325)
					{
						towers.add(new LaserTower(turretX, turretY));
						money -= 325;
					}
					else
					{
						info1 = "You do not have enough money.";
						info2 = "You need $325";
						infoTime = 120;
					}
					break;
				}
			
			chosenTurret = 0;
			
			
			
			if(chosenButton > 0 && chosenButton < 4)
				chosenTurret = chosenButton;
			

		}
		else if(!Mouse.isButtonDown(0))
		{
			isClicked = false;
		}
		
		if(Mouse.isButtonDown(1))
		{
			chosenTurret = 0;
			
			
		}
	}
}
