import java.util.Random;


public class Monster
{
	public float x;
	public float y;
	public int direction; // 0:top 1:right..
	public int animation;
	public boolean animationDir;
	public int health;
	
	public boolean active;
	public int grid[][] = new int[21][8];

	public Monster()
	{
		Random rand = new Random();
		
		active = true;
		animation = 200;
		animationDir = false;
		
		if(rand.nextBoolean())
			x = -1024;
		else
			x = 2048;
		
		y = 200;
	}
	
	public void move()
	{		
		
		if(Global.speed == 4 && ((int)x%4 != 0 || (int)y%4 != 0))
		{
			switch(direction)
			{
			case 0:
				y = 4*((int)y/4) +4;
				break;
			case 1:
				x = 4*((int)x/4) +4;
				break;
			case 2:
				y = 4*((int)y/4);
				break;
			case 3:
				x = 4*((int)x/4);
				break;
			}
		}
		
		if((int)x%64==0 && (int)y%64 == 0)
		{
			if(x<0)
			{
				boolean flag = false;
				Random rand = new Random();
				int ty = -1;
				
				while(!flag)
				{
					ty = rand.nextInt(8);
					if(!Game.map.grid[0][ty])
						flag = true;
				}
				
				this.y = ty*64 + 128;
				this.x = -63;
				direction = 1;
			}
			else if(x>1280)
			{
				boolean flag = false;
				Random rand = new Random();
				int ty = -1;
				
				while(!flag)
				{
					ty = rand.nextInt(8);
					if(!Game.map.grid[20][ty])
						flag = true;
				}
				
				this.y = ty*64 +128;
				this.x = 1343;
				direction = 3;			
			}
			else
			{
				for(int i=0; i<21; i++)
					for(int j=0; j<8; j++)
						grid[i][j] = 100;
				
				grid[(int)(x/64)][(int)(y/64)-2] = 0;
				
				
				boolean flag = false;
				boolean found = false;
				
				while(!flag && !found)
				{
					flag = true;
					
					for(int i=0; i<21; i++)
						for(int j=0; j<8; j++)
							if(grid[i][j]<100)
							{	
								if(found)
									break;
								
								for(Base base:Game.map.bases)
								{

									if((base.x/64 == i && base.y/64 == j+1) || (base.x/64 == i && base.y/64 == j+2))
									{

										int index;
										
										while(!found)
										{										
											index = 4;
											
											if(Game.map.type == 2)
												if(j == 4)
													if(i == 5)
														i = 15;
													else if(i == 15)
														i = 5;
											
											if(j>0)
												if(grid[i][j-1] == grid[i][j]-1)
												{
													index = 0;
												}
											
											if(i<20)
												if(grid[i+1][j] == grid[i][j]-1)
												{
													index = 1;
												}	
											
											if(j<7)
												if(grid[i][j+1] == grid[i][j]-1)
												{
													index = 2;
												}
											
											if(i>0)
												if(grid[i-1][j] == grid[i][j]-1)
												{
													index = 3;
												}

											
											switch(index)
											{
											case 0:
												j--;
												break;
											case 1:
												i++;
												break;
											case 2:
												j++;
												break;
											case 3:
												i--;
												break;
											}
											
											if(grid[i][j] == 0)
											{
												switch(index)
												{
												case 0:
													direction = 2;
													break;
												case 1:
													direction = 3;
													break;
												case 2:
													direction = 0;
													break;
												case 3:
													direction = 1;
													break;
												}
												
												found = true;
											}
											
										}								

									}
								}
								
								if(i>0)
									if(grid[i-1][j] > grid[i][j]+1  && !Game.map.grid[i-1][j])
									{
										grid[i-1][j] = grid[i][j]+1;
										flag = false;
									}
								
								if(i<20)
									if(grid[i+1][j] > grid[i][j]+1 && !Game.map.grid[i+1][j])
									{
										grid[i+1][j] = grid[i][j]+1;
										flag = false;
									}
								
								if(j>0)
									if(grid[i][j-1] > grid[i][j]+1 && !Game.map.grid[i][j-1])
									{
										grid[i][j-1] = grid[i][j]+1;
										flag = false;
									}
								
								if(j<7)
									if(grid[i][j+1] > grid[i][j]+1 && !Game.map.grid[i][j+1])
									{
										grid[i][j+1] = grid[i][j]+1;
										flag = false;
									}
													
							}
				}
			}
		}
			
			
		switch(direction)
		{
		case 0:
			y -= Global.speed;
			break;
		case 1:
			x += Global.speed;
			break;
		case 2:
			y += Global.speed;
			break;
		case 3:
			x -= Global.speed;
			break;
		}
		
		if(Game.map.type==2)
			if(y == 384)
				if(x == 320)
					x = 960;
				else if(x == 960)
					x = 320;
		
		if(animation>200)
			animationDir = false;
		else if(animation <40)
			animationDir = true;
		
		if(animationDir)
			animation += Global.speed*10;
		else
			animation -= Global.speed*10;
		
		if(health<0)
			active = false;
	}
	
}
