import java.io.FileWriter;
import java.io.IOException;


public class Settings
{
	public static int width = 800;
	public static int height = 450;
	public static int colorDepth = 32;
	public static boolean isFullScreen = false;
	public static boolean antiAliasing = false;
	public static int vsync = 50;
	
	public static void setDisplay(int _width, int _height, boolean _isFullScreen)
	{
		width = _width;
		height = _height;
		isFullScreen = _isFullScreen;
	}
	
	public static void saveFile()
	{
		FileWriter file = null;
		
		try
		{
			file = new FileWriter("cfg/settings.knx");
			file.write(width/128);
			file.write(width%128);
			file.write(height/128);
			file.write(height%128);
			
			int data = 0;
			
			if(isFullScreen)
				data+=1;
			
			if(antiAliasing)
				data+=2;
			
			switch(vsync)
			{
			case 60:
				data+=4;
				break;
			case 50:
				data+=8;
				break;
			case 30:
				data+=12;
				break;
			default:
				break;
			}
			
			file.write(data);
			
			data = 0;
			
			switch(colorDepth)
			{
			case 16:
				data+=1;
				break;
			case 24:
				data+=2;
				break;
			case 32:
				data+=3;
				break;
			default:
				break;
			}
			
			file.close();
			
			
		}
		catch (IOException e)
		{
			Error.code(3);
		}
		
	}
	
}
